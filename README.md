# momos-api

## Requirements

* Node 14
* Git
* Express.JS (https://expressjs.com/)
* Sequelize (https://sequelize.org/)
* express-sequelize-crud (https://www.npmjs.com/package/express-sequelize-crud)

## Common Setup

Clone the repo and install the dependencies.

```bash
git clone project
cd momos-api
```

```bash
npm install
```

## Expose Environment
create .env in root of the project folder

```bash
PORT=3060
DB_HOST=db
DB_PORT=5432
DB_NAME=momos
DB_USER=momos
DB_PASSWORD=momos
DB_DIALECT=postgres
API_KEY=0f3e84fe7456ae95b838135d6f7d3c92
API_URL=https://api.themoviedb.org/3
```

Similarly create .env.test for test db configuration and .env.production for production db configuration

## Run Project

To start the express server with development environment, run the following

```bash
docker-compose up --build
```

## To setup database

```bash
environment:
    - POSTGRES_USER=momos
    - POSTGRES_PASSWORD=momos
    - POSTGRES_DB=momos
```

Follow the steps:
* Create a database (e.g. momos) for that user.
* Execute the sql statements in docs/db.sql

## Adminer for database access
```
127.0.0.1:7009
```