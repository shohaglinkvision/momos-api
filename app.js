import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import requestIp from 'request-ip';
import rateLimiterMiddleware from './server/middlewares/rate-limiter';
import routes from './server/routes';
import cron from 'node-cron'
import Schedule from './server/tasks';

const app = express();

// Schedule tasks to be run on the server.
Schedule(cron)


app.use(helmet());
app.use(requestIp.mw());
app.use(rateLimiterMiddleware);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());

app.use(cors());

app.use(routes);

export default app;
