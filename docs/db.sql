create table if not exists popular_movies (
    id serial PRIMARY KEY,
    adult boolean,
    backdrop_path varchar(255),
    title varchar(255),
    original_language varchar(255) ,
    original_title varchar(255) ,
    overview text,
    release_date TIMESTAMP ,
    poster_path varchar(255),
    popularity float ,
    vote_average float  ,
    vote_count int,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
)
