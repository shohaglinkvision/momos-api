FROM node:14
# Create app directory
RUN mkdir -p /var/www/momos

WORKDIR /var/www/momos

# Install app dependencies
COPY package*.json ./


RUN npm install 
RUN npm i winston

# Bundle app source
COPY . /var/www/momos

WORKDIR /var/www/momos


ARG NODE_ENV=dev
ENV NODE_ENV=${NODE_ENV}

EXPOSE 3060
CMD ["npm", "start"]