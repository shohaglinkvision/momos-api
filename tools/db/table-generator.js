import dotenv from "dotenv";
import knex from "knex";
import path from "path";
import fs from "fs";

if (process.env.DOTENV_CONFIG_PATH) {
  const dotenvConfigPath = path.resolve(
    process.cwd(),
    process.env.DOTENV_CONFIG_PATH
  );
  dotenv.config({ path: dotenvConfigPath });
} else {
  dotenv.config();
}

const db = knex({
  client: "pg",
  connection: {
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
  },
});

const tables = JSON.parse(
  fs.readFileSync(`${process.cwd()}/tools/db/tables.json`)
);

Object.keys(tables).forEach((tableName) => {
  console.info(`Checking if "${tableName}" exists.`);
  db.schema
    .hasTable(tableName)
    .then(function (exists) {
      if (!exists) {
        console.info(
          `"${tableName}" does not exist. Creating table "${tableName}"`
        );
        const table = tables[tableName];
        db.schema.createTable(tableName, function (t) {
          Object.keys(table).forEach((colName) => {
            const colConfig = table[colName];
            const col = table[colConfig["type"]](colName);
            if (colConfig["pk"]) {
              col.primary();
            }
            if (colConfig["nutnull"]) {
              col.notNullable();
            }
          });
        });
      }
    })
    .then(() => {
      console.info(`"${tableName}" has been created successfully.`);
    })
    .catch((err) => {
      console.error(err);
    });
});

// db.schema.hasTable("organization").then(function (exists) {
//   if (!exists) {
//     db.schema.createTable("organization", function (t) {
//       table.uuid("org_id").notNullable();
//       table.string("org_name").notNullable();
//       table.text("description");
//       table.datetime("created_at");
//       table.datetime("updated_at");
//     });
//   }
// });
