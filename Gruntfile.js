module.exports = function (grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    env: {
      main: {
        src: ".env"
      }
    },
    exec: {
      sequelize_models: {
        cmd: 'sequelize-auto -o "./server/models" -d <%= DB %> -h <%= HOST %> -u <%= USER %> -p 5432 -x <%= PASSWORD %> -e postgres'
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-env');
  grunt.loadNpmTasks('grunt-exec');

  // Default task(s).
  grunt.registerTask('loadconst', 'Load constants', function () {
    grunt.config('HOST', process.env.POSTGRES_HOST);
    grunt.config('PORT', process.env.POSTGRES_PORT);
    grunt.config('DB', process.env.POSTGRES_DB);
    grunt.config('USER', process.env.POSTGRES_USER);
    grunt.config('PASSWORD', process.env.POSTGRES_PASSWORD);
  });

  grunt.registerTask('sequelize:models', ['env', 'loadconst', 'exec:sequelize_models']);
};
