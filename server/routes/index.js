import express from 'express';
import _ from 'lodash';
import crud from '../middlewares/crud';
import models from '../models';
import logger from '../modules/logger';

const router = express.Router();

Object.keys(models)
  .filter((modelName) => ['sequelize', 'Sequelize'].indexOf(modelName) === -1)
  .forEach((modelName) => {
    router.use(crud(`/${_.kebabCase(modelName)}`, models[modelName]));
  });

router.use((err, req, res, next) => {
  logger.error(err);
  if (err) {
    res.status(err.status || 400).send({ error: err.message });
  } else {
    next();
  }
});

export default router;
