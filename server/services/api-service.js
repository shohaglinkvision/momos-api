import axios from 'axios';
const API_KEY = process.env.API_KEY

let client = axios.create({
  baseURL: process.env.API_URL,
  headers: {
    'Content-Type': 'application/json'
  },
});

const get = async (url, params) => {
  params['api_key'] = API_KEY;
  const resp = await client.get(url, { params });
  return resp.data;
};

const post = async (url, body) => {
  const resp = await client.post(url, body);
  return resp.data;
};

const getPopularMovies = () => get('/discover/movie', {sort_by : "popularity.desc"});

export default client;
export {
  getPopularMovies,
};
