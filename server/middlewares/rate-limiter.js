import rateLimiterFlexible from 'rate-limiter-flexible';
import logger from '../modules/logger';

const rateLimiter = new rateLimiterFlexible.RateLimiterMemory({
  points: 60,
  duration: 60,
});

const rateLimiterMiddleware = (req, res, next) => {
  rateLimiter
    .consume(req.clientIp)
    .then(() => {
      next();
    })
    .catch(() => {
      logger.warn('Too many requests from :: ', req.clientIp);
      res.status(429).send('Too Many Requests');
    });
};

export default rateLimiterMiddleware;
