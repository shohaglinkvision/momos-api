import bodyParser from 'body-parser';
import express from 'express';
import _ from 'lodash';
import qs from 'querystring';
import logger from '../modules/logger';
import HttpStatus from '../shared/utility/http-status';

const crud = (path, model, config = {}) => {
  const router = express.Router();
  const VALID_FIELDS = Object.keys(model.rawAttributes);

  router.use(bodyParser.json());

  const selfLinkInjector = (req, item) => {
    const tmpItem = item;
    tmpItem.links = item.links || {};
    tmpItem.links.self = `${req.protocol}://${req.get('host')}${req.path}`;
    if (tmpItem.links.self[tmpItem.links.self.length - 1] !== '/') {
      tmpItem.links.self = `${tmpItem.links.self}/`;
    }
    tmpItem.links.self = `${tmpItem.links.self}${item[req.pkFieldName]}`;
    return tmpItem;
  };

  const pkFieldNameResolver = (req, res, next) => {
    [req.pkFieldName] = model.primaryKeyAttributes;
    next();
  };
  const whereClauseInjector = (req, res, next) => {
    req.where = {};
    _.intersection(Object.keys(req.query), VALID_FIELDS).forEach((p) => {
      req.where[p] = req.query[p];
    });
    next();
  };

  const orderClauseInjector = (req, res, next) => {
    if (req.query.sort) {
      const sorts = _.isArray(req.query.sort)
        ? req.query.sort
        : [req.query.sort];
      req.order = sorts
        .map((sort) => sort.split(':'))
        .map((sort) => [sort[0], sort[1] || 'ASC']);
    }
    next();
  };

  const pkWhereClauseInjector = (req, res, next) => {
    if (req.params.id) {
      req.where = req.where || {};
      req.where[model.primaryKeyAttributes[0]] = req.params.id;
    }
    next();
  };

  const buildPrevLink = (req, offset, limit) => {
    if (offset > 0) {
      req.query.offset = offset - limit;
      if (req.query.offset === 0) {
        delete req.query.offset;
      }
      return `${req.protocol}://${req.get('host')}${req.path}?${qs.stringify(req.query)}`;
    }
    return '';
  };

  const buildNextLink = (req, offset, limit, total) => {
    if ((offset + 1) * limit < total) {
      req.query.offset = offset + limit;
      return `${req.protocol}://${req.get('host')}${req.path}?${qs.stringify(req.query)}`;
    }
    return '';
  };

  router.get(path,
    [pkFieldNameResolver, whereClauseInjector, orderClauseInjector],
    (req, res, next) => {
      const limit = _.parseInt(req.query.limit || config.limit || 100);
      const offset = _.parseInt(req.query.offset || 0);
      const startTime = Date.now();
      model
        .findAndCountAll({
          where: req.where,
          offset,
          limit,
          order: req.order,
          raw: true,
        })
        .then(({ count, rows }) => {
          const resp = {
            items: rows.map((row) => selfLinkInjector(req, row)),
            pageInfo: {
              totalResults: count,
              resultsPerPage: limit,
            },
            _links: {
              prev: buildPrevLink(req, offset, limit),
              self: `${req.protocol}://${req.get('host')}${req.originalUrl}`,
              next: buildNextLink(req, offset, limit, count),
            },
          };
          res.set('X-QUERY-EXEC-TIME', ((Date.now() - startTime) / 1000));
          res.send(resp);
        })
        .catch((err) => {
          logger.error(err);
          next(err);
        });
    });

  router.get(`${path}/:id`, (req, res, next) => {
    model
      .findByPk(req.params.id)
      .then((result) => res.send(result))
      .catch((err) => {
        logger.error(err);
        next(err);
      });
  });

  router.post(path, (req, res, next) => {
    model
      .build(req.body)
      .save()
      .then((result) => res.status(201).send(result))
      .catch((err) => {
        logger.error(err);
        next(err);
      });
  });

  router.put(`${path}/:id`, pkWhereClauseInjector, async (req, res, next) => {
    try {
      const obj = await model.findByPk(req.params.id);
      if (obj) {
        model
          .update(req.body, { where: req.where })
          .then((result) => res.send(result))
          .catch((err) => {
            logger.error(err);
            res.sendStatus(HttpStatus.BAD_REQUEST);
          });
      } else {
        res.sendStatus(HttpStatus.NOT_FOUND);
      }
    } catch (err) {
      logger.error(err);
      next(err);
    }
  });

  router.delete(`${path}/:id`, pkWhereClauseInjector, (req, res, next) => {
    model
      .destroy({ where: req.where })
      .then(() => res.sendStatus(HttpStatus.NO_CONTENT))
      .catch((err) => {
        logger.error(err);
        next(err);
      });
  });

  return router;
};

export default crud;
