import Sequelize from 'sequelize';
import config from '../config/db';
import PopularMovie from './popular_movie';

const db = {};
const models = {
  PopularMovie
};
const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  {
    host: config.host,
    dialect: config.dialect,
    define: {
      underscored: true,
    },
  },
);

Object.keys(models).forEach((modelName) => {
  const model = models[modelName](sequelize, Sequelize.DataTypes);
  db[model.name] = model;
});

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
