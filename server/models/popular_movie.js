export default function PopularMovie(sequelize, DataTypes) {
  return sequelize.define('popular_movies', {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      adult: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        default: false
      },
      backdrop_path: {
        type: DataTypes.STRING,
        allowNull: true
      },
      original_language: {
        type: DataTypes.STRING,
        allowNull: true
      },
      original_title: {
        type: DataTypes.STRING,
        allowNull: true
      },
      overview: {
        type: DataTypes.STRING,
        allowNull: true
      },
      release_date: {
        type: DataTypes.DATE,
        allowNull: true
      },
      poster_path: {
        type: DataTypes.STRING,
        allowNull: true
      },
      popularity: {
        type: DataTypes.INTEGER,
        allowNull: true,
        default: 0
      },
      title: {
        type: DataTypes.STRING,
        allowNull: true
      },
      vote_average: {
        type: DataTypes.FLOAT,
        allowNull: true,
        default: 0
      },
      vote_count: {
        type: DataTypes.INTEGER,
        allowNull: false,
        default: 0
      }
    
  }, {
    sequelize,
    tableName: 'popular_movies',
  });
}
