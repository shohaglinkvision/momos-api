import http from 'http';
import logger from '../../modules/logger';
import app from '../../../app';

const port = process.env.PORT || 3060;

const initializeAppServer = function initializeAppServer(callback) {
  logger.info('App server - Initializing...');

  return http.createServer(app).listen(port, (err) => {
    if (err) {
      logger.info('App server - Initialization failed');
      process.exit();
    }

    logger.info(`App server - Initialized on port ${port}`);
    callback();
  });
};

const initialize = function initialize() {
  const server = initializeAppServer(() => {
    logger.info('Done initializing server');
  });

  server.on('error', (error) => {
    logger.error('Error in server :', error);
  });
};

initialize();
