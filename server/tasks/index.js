import popularMovie from "./PopularMovie";

const Schedule = (cron) => {
    cron.schedule('00 00 12 * * 0-6', function() {
        popularMovie()
    });
}
export default Schedule;