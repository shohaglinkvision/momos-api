import { getPopularMovies } from "../services/api-service"
import models from '../models';
const { sequelize } = models;
import _ from 'lodash';


const addMovieInDB = async(movies) => {
    const t = await sequelize.transaction();
    await Promise.all(
        movies.results.map(async (movie) => {
            try {
                let _movie = await models.popular_movies.findOne({
                    where: {
                      id: movie.id
                    },
                });
                if(!_movie) {
                    await models.popular_movies
                    .create(
                    _.pick(movie, [
                    'id',
                    'adult',
                    'backdrop_path',
                    'original_language',
                    'overview',
                    'original_title',
                    'release_date',
                    'poster_path',
                    'popularity',
                    'title',
                    'vote_average',
                    'vote_count'
                    ]),
                    { transaction: t },
                )
                .then((r) => r.get({ plain: true }));
                }
            } catch (error) {
                console.log(error);
                await t.rollback();
                throw error;
            }
        })
    )
    await t.commit();
}
const popularMovie = async() => { 
    const popularMovies = await getPopularMovies()
    await addMovieInDB(popularMovies)
}
export default popularMovie
