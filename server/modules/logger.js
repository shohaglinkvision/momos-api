import winston from 'winston';
// import 'winston-daily-rotate-file';

const logger = winston.createLogger({
  format: winston.format.json(),
  transports: [],
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
    }),
  );
}

// if (process.env.LOCAL !== 'true') {
//   logger.add(
//     new winston.transports.DailyRotateFile({
//       filename: '/tmp/entity-manager-api-%DATE%.log',
//       datePattern: 'YYYY-MM-DD-HH',
//       zippedArchive: true,
//       maxSize: '20m',
//       maxFiles: '14d',
//     }),
//   );
// }

export default logger;
