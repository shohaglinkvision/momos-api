import path from 'path';
import dotenv from 'dotenv';

if (process.env.DOTENV_CONFIG_PATH) {
  const dotenvConfigPath = path.resolve(process.cwd(), process.env.DOTENV_CONFIG_PATH);
  dotenv.config({ path: dotenvConfigPath });
} else {
  dotenv.config();
}

const config = {
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  dialect: process.env.DB_DIALECT,
};

export default config;
